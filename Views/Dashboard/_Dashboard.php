<?php
?>
<!-- page content -->
<div class="right_col" role="main">

    <div class="clearfix"></div>  
    <div class="x_panel">
            <div class="x_content">
                <!--<br />-->
                <input type="hidden" name="DocType" id="DocType" value="DASH">

                <div class="row top_tiles">
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats" id="heading1">
                            <div class="icon"><i class="fa fa-user"></i></div>
                            <div class="count">

                                <div id="TotalOwner"></div>

                            </div>
                            <h3>মালিকের সংখ্যা</h3>
                            <!-- <p><a href="./Owner.php">বিস্তারিত দেখুন>></a></p> -->
                            <p>&nbsp</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-users"></i></div>
                            <div class="count">

                                <div id="TotalDriver"></div>

                            </div>
                            <h3>চালকের সংখ্যা </h3>
                           <!--  <p><a href="./Driver.php">বিস্তারিত দেখুন>></a></p> -->
                           <p>&nbsp</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-car"></i></div>
                            <div class="count">
                                <div id="TotalVehicle"></div>
                            </div>
                            <h3>গাড়ির সংখ্যা </h3>
                            <!-- <p><a href="./Vehicle.php">বিস্তারিত দেখুন>></a></p> -->
                            <p>&nbsp</p>
                        </div>
                    </div>
                </div>

                <div class="row top_tiles">
                    <!-- <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-calendar"></i></div>
                            <div class="count">
                                   <div id="FiscalYearD"></div>
                            </div>
                            <h3>অর্থবছর</h3>
                            <p> &nbsp &nbsp  &nbsp &nbsp  </p>
                        </div>
                    </div> -->
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-money"></i></div>
                            <div class="count">
                                   <div id="TotalDemand"></div>
                            </div>
                            <h3>সর্বমোট আদায়</h3>
                            <!-- <p><a href="./AllBill.php">বিস্তারিত দেখুন>></a></p> -->
                            <p>&nbsp</p>
                        </div>
                    </div>
                    <div class="animated flipInY col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="tile-stats">
                            <div class="icon"><i class="fa fa-credit-card"></i></div>
                            <div class="count">
                                   <div id="TotalDue"></div>
                            </div>
                            <h3>সর্বমোট বকেয়া</h3>
                            <!-- <p><a href="./AllBill.php">বিস্তারিত দেখুন>></a></p> -->
                            <p>&nbsp</p>
                        </div>
                    </div>
                </div>
            </div>
    </div>

</div>
<!-- /page content -->